import scikit_filter_bank_simulation.util.filter_analysis_tools
from scikit_filter_bank_simulation.util.filter_bank_model_tools import (
    FilterBankModelTools,
)
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate

data_file = np.load("r200_microstrip_data_arrays.npz")
microstrip_width_array = data_file["microstrip_width_array"]
dielectric_thickness_array = data_file["dielectric_thickness_array"]
mean_port_z0_array = data_file["mean_port_z0_array"]
mean_port_permittivity_array = data_file["mean_port_permittivity_array"]

x = dielectric_thickness_array * 1e9
y = microstrip_width_array * 1e6
z = np.real(mean_port_z0_array)

fig = plt.figure()

grid_x, grid_y = np.meshgrid(x, y)

ax = plt.axes(projection='3d')
ax.plot_trisurf(x, y, z,
                cmap='viridis', edgecolor='none');
ax.set_title('surface');
ax.set_xlabel("dielectric thickness (nm)")
ax.set_ylabel("Microstrip Width (um)")
ax.set_zlabel("Mean Port Impedance ($\Omega$)")
plt.show()

x = dielectric_thickness_array * 1e9
y = microstrip_width_array * 1e6
z = np.real(mean_port_permittivity_array)

fig = plt.figure()

grid_x, grid_y = np.meshgrid(x, y)

ax = plt.axes(projection='3d')
ax.plot_trisurf(x, y, z,
                cmap='viridis', edgecolor='none');
ax.set_title('surface');
ax.set_xlabel("dielectric thickness (nm)")
ax.set_ylabel("Microstrip Width (um)")
ax.set_zlabel("Mean Port $E_{eff}$ ($\Omega$)")
plt.show()

microstrip_width = 2.0e-6
dielectric_thickness = 300e-9
z0_value = interpolate.griddata(
    (microstrip_width_array, dielectric_thickness_array),
    mean_port_z0_array,
    (microstrip_width, dielectric_thickness),
)

print(z0_value)
