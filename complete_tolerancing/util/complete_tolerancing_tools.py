import numpy as np
import skrf as rf
import matplotlib.pyplot as plt

from complete_tolerancing.model.FilterBankData import FilterBankData
from scikit_filter_bank_simulation.util.filter_bank_model_tools import (
    FilterBankModelTools,
)


class CompleteTolerancingTools:
    @staticmethod
    def get_filter_bank_data(
        oversampling: float,
        resolution: float, # Either 200 or 300
        loss_tangent: float,
        filter_spacing: float,
        frequency_min: int,
        frequency_max: int,
        start: int,
        stop: int,
        number_of_points: int,
    ) -> FilterBankData:
        def thickness_error(x):
            """
            Return the SiN thickness error for a filter at a given x value where x is the position in the filter bank as a
            percentage. e.g first filter, x = 0. Last filter, x = 100. etc.
            :param x:
            :return:
            """
            return (-(((4 * x / 100) + 1) ** 2.5) + 22) * 1e-9

        def etch_error(x):
            """
            returns the etch error for a filter at a given x value where x is the position in the filter bank as a percentage.
            e.g first filter, x = 0. Last filter, x = 100. etc.
            :param x:
            :return:
            """
            return (1 / (((3 * x / 100) + 0.2) ** (1 / 16)) - 0.99) * 1e-6

        # Define readout frequency band:
        frequency_band = rf.Frequency(
            start=start, stop=stop, unit="GHz", npoints=number_of_points
        )

        number_of_channels = round(
            oversampling * resolution * np.log(frequency_max / frequency_min)
        )
        target_f0_array = FilterBankModelTools.create_target_f0_array(
            frequency_min, frequency_max, number_of_channels
        )

        # Block to create a thicknessArray and etchErrorArray using MUSCAT data based on position.
        position_array = np.linspace(0, 100, number_of_channels)
        dielectric_thickness_array = np.empty(position_array.shape)
        etch_error_array = np.empty(position_array.shape)
        for count, x in enumerate(position_array):
            dielectric_thickness = 500e-9
            if resolution == 200:
                dielectric_thickness = 300e-9
            dielectric_thickness_array[count] = dielectric_thickness + thickness_error(x)
            etch_error_array[count] = etch_error(x)

        realistic_filter_bank_s_params = FilterBankModelTools.get_filter_bank_s_params(
            frequency_band=frequency_band,
            target_f0_array=target_f0_array,
            resolution=resolution,
            etch_error_array=etch_error_array,
            dielectric_thickness_array=dielectric_thickness_array,
            loss_tangent=loss_tangent,
            filter_spacing=filter_spacing,
            plot_circuit_map=False,
        )

        dielectric_thickness = 500e-9
        if resolution == 200:
            dielectric_thickness = 300e-9
        ideal_filter_bank_s_params = FilterBankModelTools.get_filter_bank_s_params(
            frequency_band=frequency_band,
            target_f0_array=target_f0_array,
            resolution=resolution,
            etch_error_array=np.zeros(shape=target_f0_array.shape),
            dielectric_thickness_array=np.full(target_f0_array.shape, dielectric_thickness),
            loss_tangent=0.0,
            filter_spacing=filter_spacing,
            plot_circuit_map=False,
        )

        return FilterBankData(
            frequency_array=frequency_band.f,
            realistic_filter_bank_s_params=realistic_filter_bank_s_params,
            ideal_filter_bank_s_params=ideal_filter_bank_s_params,
            frequency_min=frequency_min,
            frequency_max=frequency_max,
            number_of_channels=number_of_channels,
        )

    @staticmethod
    def plot_filter_bank_data(
        frequency_array: np.ndarray,
        realistic_filter_bank_s_params: np.ndarray,
        ideal_filter_bank_s_params: np.ndarray,
        frequency_min: float,
        frequency_max: float,
        save_figure: bool,
    ):
        fig = plt.figure(figsize=(10, 3))
        colour_map = plt.get_cmap("gist_rainbow_r")
        colors = [
            colour_map(i)
            for i in np.linspace(0, 1, realistic_filter_bank_s_params.shape[0])
        ]

        # Begin counting total transmission.
        non_ideal_total_power = np.zeros(realistic_filter_bank_s_params[0].shape)
        non_ideal_channel_total_power = np.zeros(
            realistic_filter_bank_s_params[0].shape
        )
        for index, s_param in enumerate(np.abs(realistic_filter_bank_s_params)):
            if index == 0 or index == 1:
                non_ideal_total_power = non_ideal_total_power + s_param**2
                continue
            non_ideal_total_power = non_ideal_total_power + s_param**2
            non_ideal_channel_total_power = non_ideal_channel_total_power + s_param**2

            plt.plot(
                frequency_array * 1e-9,
                s_param**2,
                color=colors[index],
            )

        # Repeat for ideal filter-bank:
        ideal_total_power = np.zeros(ideal_filter_bank_s_params[0].shape)
        ideal_channel_total_power = np.zeros(ideal_filter_bank_s_params[0].shape)

        for index, s_param in enumerate(np.abs(ideal_filter_bank_s_params)):
            if index == 0 or index == 1:
                ideal_total_power = ideal_total_power + s_param**2
                continue

            ideal_total_power = ideal_total_power + s_param**2
            ideal_channel_total_power = ideal_channel_total_power + s_param**2

            if index == 2:
                label = "Ideal $|S_{i1}|^2$"
            else:
                label = ""
            plt.plot(
                frequency_array * 1e-9,
                s_param**2,
                linestyle="dotted",
                color="0.75",
                label=label,
            )

        plt.plot(
            frequency_array * 1e-9,
            non_ideal_channel_total_power,
            linestyle="-",
            color="k",
            label="Realistic $\\Sigma_{i=3}^{N+2} |S_{i1}|^2$",
        )

        plt.plot(
            frequency_array * 1e-9,
            ideal_channel_total_power,
            linestyle="-.",
            color="0.75",
            label="Ideal $\\Sigma_{i=3}^{N+2} |S_{i1}|^2$",
        )
        plt.vlines(
            [frequency_min * 1e-9, frequency_max * 1e-9],
            ymin=0.0,
            ymax=1.0,
            color="r",
            linestyle="--",
            label="Target frequency\nbounds",
        )

        plt.xlabel("Frequency (GHz)", fontsize=18)
        plt.ylabel("Power Transmission", fontsize=16)
        plt.tick_params("both", labelsize=14)
        # plt.xlim(frequency_min, frequency_max)
        plt.legend(fontsize=14, loc="center right", bbox_to_anchor=(1.35, 0.5))
        plt.show()

        if save_figure:
            fig.savefig(
                f"plots\\FilterBank{frequency_min*1e-9}-{frequency_max*1e-9}GHzWithFullFabError.png",
                bbox_inches="tight",
            )

    @staticmethod
    def get_total_power_arrays(
        filter_bank_s_params: np.ndarray,
    ):
        """
        Function to extract the total channel power and the total power (including S11 and S21)from a filter_bank
        S-parameter array.

        :param frequency_array: array of frequency data points.
        :param filter_bank_s_params: array of filter bank s parameter data points.

        :return total_power array, and channel_total_power array.
        """

        # Begin counting total transmission.
        total_power = np.zeros(filter_bank_s_params[0].shape)
        channel_total_power = np.zeros(filter_bank_s_params[0].shape)
        for index, s_param in enumerate(np.abs(filter_bank_s_params)):
            if index == 0 or index == 1:
                total_power = total_power + s_param**2
                continue
            channel_total_power = channel_total_power + s_param**2

        return total_power, channel_total_power
