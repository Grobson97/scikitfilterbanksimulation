import scikit_filter_bank_simulation.util.filter_analysis_tools
from scikit_filter_bank_simulation.util.filter_bank_model_tools import (
    FilterBankModelTools,
)
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
import os
from lmfit import Model
from lmfit import Parameters

def coupling_capacitor_s21(
        data_frequency_array: np.ndarray,
        capacitance: float,
        parasitic_inductance: float,
        length: float,
):

    # Define frequency band:
    frequency_band = rf.Frequency(
        start=data_frequency_array[0],
        stop=data_frequency_array[-1],
        unit="Hz",
        npoints=data_frequency_array.size,
    )

    microstrip_media = FilterBankModelTools.create_media(
        frequency_band=frequency_band,
        microstrip_width=2.5e-6,
        loss_tangent=0.0,
        dielectric_thickness=500e-9,
    )
    line = microstrip_media.line(d=length, unit="um")

    # Define Circuit components
    capacitor = microstrip_media.capacitor(C=capacitance, name="C")
    parasitic_inductor = microstrip_media.inductor(L=parasitic_inductance, name="Parasitic L")

    coupling_capacitor_network = line ** parasitic_inductor ** capacitor ** parasitic_inductor ** line

    coupling_capacitor_network.renormalize(
            [
                complex(50, 0),
                complex(50, 0),
            ]
        )

    return coupling_capacitor_network.s_db[:, 0, 1]


directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\SPT-SLIM\ABCDModelTests\CouplingCapacitor\Coupling_capacitor_width_sin_thickness_sweep"

fit_dictionary = {
    "dielectric_thickness": [],
    "capacitor_width": [],
    "capacitance": [],
    "parasitic_inductance": [],
}

for count, file in enumerate(os.listdir(directory)):
    filename = os.fsdecode(file)
    file_path = os.path.join(directory, filename)

    if ".s2p" in filename:
        # Read current touchstone file
        touchstone_file = rf.Touchstone(file_path)
        # Get touchstone variables from comments
        variables = touchstone_file.get_comment_variables()

        # Make string ccontaining file parameters:
        variables_string = ""
        for variable in variables:
            if variable == list(variables.keys())[-1]:
                variables_string += variable + "=" + str(variables[variable][0])
                continue
            variables_string += variable + "=" + str(variables[variable][0]) + ", "

        # Extract variables from header
        capacitor_width = float(variables["capIn"][0])
        sin_thickness = float(variables["SiNThickness"][0])

        # Renormalise port impedances.
        network = rf.Network(file_path)
        frequency = network.f

        # Fit to parasitic capacitance to data:
        # Define model:
        model = Model(coupling_capacitor_s21)

        # Define Parameters:
        params = Parameters()
        params.add("capacitance", value=1e-15, vary=True)
        params.add("parasitic_inductance", value=1e-12, min=0, vary=True)
        params.add("length", value=21.25, vary=True)

        result = model.fit(
            network.s_db[:, 0, 1],
            params,
            data_frequency_array=network.f,
        )

        ideal = result.best_fit
        fit_capacitance = result.best_values["capacitance"]
        fit_parasitic_inductance = result.best_values["parasitic_inductance"]
        d = result.best_values["length"]

        fit_dictionary["dielectric_thickness"].append(sin_thickness)
        fit_dictionary["capacitor_width"].append(capacitor_width)
        fit_dictionary["capacitance"].append(fit_capacitance)
        fit_dictionary["parasitic_inductance"].append(fit_parasitic_inductance)

        # plt.figure(figsize=(8, 6))
        # plt.plot(network.f * 1e-9, network.s_db[:, 0, 1], linestyle="none", marker="o")
        # plt.plot(network.f * 1e-9, ideal, color="r", label=f"Best fit with:\nC={fit_capacitance}\nL={fit_parasitic_inductance}\nd={d}")
        # plt.xlabel("Frequency (GHz)")
        # plt.ylabel("S21 (dB)")
        # plt.legend()
        # plt.show()

        print(f"file {count} complete")

for key in fit_dictionary:
    fit_dictionary[key] = np.array(fit_dictionary[key])

np.savez(
    "coupling_capacitor_data_arrays",
    variableCapWidthArray=fit_dictionary["capacitor_width"],
    dielectricThicknessArray=fit_dictionary["dielectric_thickness"],
    capacitanceArray=fit_dictionary["capacitance"],
    parasiticIndutanceArray=fit_dictionary["parasitic_inductance"],
)
