import numpy as np
import skrf as rf
import matplotlib.pyplot as plt
from scikit_filter_bank_simulation.util.filter_bank_model_tools import (
    FilterBankModelTools,
)


def main():
    # Define readout frequency band:
    frequency_band = rf.Frequency(start=120, stop=180, unit="GHz", npoints=101)
    loss_tangent = 1.0e-3
    target_f0 = 150.0

    feedline_media = FilterBankModelTools.create_media(
        frequency_band=frequency_band,
        microstrip_width=2.5e-6,
        loss_tangent=loss_tangent,
        dielectric_thickness=500e-9,
    )

    plt.figure(figsize=(8, 6))
    lengths = np.array([30000, 40000, 50000, 60000, 70000, 80000, 90000]) * 1e-6

    for length in lengths:

        input_line = feedline_media.line(d=length, unit="m", name="feedline line")

        plt.plot(
            frequency_band.f * 1e-9,
            100 * np.abs(input_line.s[:, 0, 1]) ** 2,
            label=f"{length*1e3:.2f}mm",
            linestyle="-",
        )

    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 Throughput Power (%)")
    plt.legend(title="Microstrip Lengths")
    plt.show()


if __name__ == "__main__":
    main()
