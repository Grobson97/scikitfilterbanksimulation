import numpy as np
import skrf as rf
import matplotlib.pyplot as plt
from scikit_filter_bank_simulation.util.filter_bank_model_tools import (
    FilterBankModelTools,
)


def create_filter_bank(
    frequency_band: rf.Frequency,
    target_f0_array: np.ndarray,
    etch_error_array: np.ndarray,
    dielectric_thickness_array: np.ndarray,
    loss_tangent: float,
    output_channel_number: int,
    plot_circuit_map=False,
) -> rf.Network:
    """Creates an rf.Network object representing a filter bank.

    :param frequency_band: instance of the frequency band defining each frequency point that will be evaluated
    :param target_f0_array: array of target F0 values used to define the geometry of each filter
    :param etch_error_array: Array of values that can be added to geometry dimensions due to over or under etching,
    e.g increase line widths. Must be same length as target_f0 array. Each value is the etch error that will be used
    for that corresponding channel
    :param dielectric_thickness_array: array of values for each channels dielectric thickness. Must be same length
    as target_f0 array
    :param loss_tangent: Dielectric loss tangent
    :param output_channel_number: The port number to be used as the output. All other channels will be terminated in a
    matched load. If 0, all channels are terminated in a matched load and output port is at the other end of the
    feedline.
    :param plot_circuit_map: Option to plot the map of the filter-bank circuit using the rf.Circuit.plot_graph()
    method (Default value = False)

    """

    interconnection_array = []
    shunt_channel_ntwk_array = []

    feedline_media = FilterBankModelTools.create_media(
        frequency_band=frequency_band,
        microstrip_width=2.5e-6,
        loss_tangent=loss_tangent,
        dielectric_thickness=500e-9,
    )
    # Define input feedline port:
    port_in = rf.Circuit.Port(
        feedline_media.frequency, name="portIn", z0=feedline_media.z0
    )

    # Create parasitic capacitance to ground network object at first channel connection
    parasitic_cap = feedline_media.capacitor(C=-7.951817997309143e-15)
    # Add a short after parasitic capacitor
    parasitic_cap_to_gnd = parasitic_cap ** feedline_media.short()
    parasitic_cap_to_gnd = feedline_media.shunt(
        parasitic_cap_to_gnd, name="Parasitic C1"
    )

    feedline_in = feedline_media.line(110e-6, "m", name="FeedIn")
    feedline_in = feedline_in**parasitic_cap_to_gnd
    feedline_out = feedline_media.line(110e-6, "m", name="FeedOut")

    for index, target_f0 in enumerate(target_f0_array):
        channel_number = index + 1
        # select etch Error and dielectric thickness for the new channel.
        etch_error = etch_error_array[index]
        dielectric_thickness = dielectric_thickness_array[index]

        # update microstrip media for new channel:
        feedline_media = FilterBankModelTools.create_media(
            frequency_band=frequency_band,
            microstrip_width=2.5e-6 + etch_error,
            loss_tangent=loss_tangent,
            dielectric_thickness=dielectric_thickness,
        )

        resonator_media = FilterBankModelTools.create_media(
            frequency_band=frequency_band,
            microstrip_width=2e-6 + etch_error,
            loss_tangent=loss_tangent,
            dielectric_thickness=dielectric_thickness,
        )

        channel_out_media = FilterBankModelTools.create_media(
            frequency_band=frequency_band,
            microstrip_width=4.5e-6 + etch_error,
            loss_tangent=loss_tangent,
            dielectric_thickness=dielectric_thickness,
        )

        # Define parameters for the SingleFilter instance:
        # target_f0 = correctTargetF0(target_f0)
        res_w = (
            -5.69e-5 * target_f0**3 + 0.033 * target_f0**2 - 7.13 * target_f0 + 669
        )
        res_length = (
            12 + 2 * res_w
        ) * 1e-6 + FilterBankModelTools.calculate_res_len_correction(target_f0)
        cap_in = (
            (0.14 * res_w - 10.0) * 1e-6
            + FilterBankModelTools.calculate_cap_in_correction(target_f0)
            + etch_error
        )
        cap_out = (
            (0.121 * res_w - 10.7) * 1e-6
            + FilterBankModelTools.calculate_cap_out_correction(target_f0)
            + etch_error
        )

        # Create 2-port network object for the channel:
        channel_ntwk = FilterBankModelTools.create_filter_channel(
            channel_number=index + 1,
            input_microstrip_media=resonator_media,
            output_microstrip_media=channel_out_media,
            cap_in=cap_in,
            cap_out=cap_out,
            dielectric_thickness=dielectric_thickness,
            res_length=res_length,
            input_length=8.25e-6,
            output_length=19.25e-6,
        )

        # If not the desired output channel, terminate channel in matched load. (Creates 1 port network)
        if channel_number != output_channel_number:
            channel_ntwk = channel_ntwk ** channel_out_media.match()

        # Convert channel network into a shunted network. increases number of ports by 1:
        shunt_channel_ntwk = feedline_media.shunt(
            channel_ntwk, name="CH" + str(index + 1)
        )

        # renumber ports for the 3 port channel network has the following indices:
        # 0: feedline input
        # 1: feedline output
        # 2: channel output
        if shunt_channel_ntwk.nports == 3:
            shunt_channel_ntwk.renumber([0, 1, 2], [2, 0, 1])

        # Create parasitic capacitance to ground network object at each filter.
        parasitic_cap = feedline_media.capacitor(C=-7.951817997309143e-15)

        # Add a short after parasitic capacitor
        parasitic_cap_to_gnd = parasitic_cap ** feedline_media.short()
        parasitic_cap_to_gnd = feedline_media.shunt(
            parasitic_cap_to_gnd, name="Parasitic C-" + str(index + 2)
        )

        # Create microstrip network object to place between filters:
        interconnection_ntwk = feedline_media.line(
            d=3 * (res_length / 2), unit="m", name="L/4-" + str(index + 1)
        )
        interconnection_ntwk = interconnection_ntwk**parasitic_cap_to_gnd

        # Append channel network objects and ports to arrays:
        interconnection_array.append(interconnection_ntwk)
        shunt_channel_ntwk_array.append(shunt_channel_ntwk)

    # Create output port at end of filter-bank or end of channel.
    if output_channel_number == 0:
        port_out = rf.Circuit.Port(
            frequency_band, name="Output Port", z0=feedline_media.z0
        )
    else:
        port_out = rf.Circuit.Port(
            frequency_band,
            name="Channel " + str(output_channel_number) + " Port",
            z0=channel_out_media.z0,
        )

    feedline_match = feedline_media.match()
    feedline_match.name = "feedline match"

    # If statement to catch the case for a single filter
    if target_f0_array.size == 1:
        connections = [
            [(port_in, 0), (feedline_in, 0)],
            [(feedline_in, 1), (shunt_channel_ntwk, 0)],
            [(shunt_channel_ntwk, 1), (feedline_out, 0)],
        ]
        if output_channel_number == 0:
            connections.append([(feedline_out, 1), (port_out, 0)])
        if output_channel_number == 1:
            connections.append([(shunt_channel_ntwk, 2), (port_out, 0)])
            connections.append([(feedline_out, 1), (feedline_match, 0)])

    else:
        # Connect input port to first section of feedline.
        connections = [[(port_in, 0), (feedline_in, 0)]]

        for index, channel in enumerate(shunt_channel_ntwk_array):
            channel_number = index + 1
            # If first channel
            if index == 0:
                connections.append([(feedline_in, 1), (channel, 0)])
                connections.append([(channel, 1), (interconnection_array[index], 0)])
                if channel_number == output_channel_number:
                    connections.append([(channel, 2), (port_out, 0)])

            # If final channel:
            elif index == len(shunt_channel_ntwk_array) - 1:
                connections.append(
                    [
                        (interconnection_array[index - 1], 1),
                        (channel, 0),
                    ]
                )
                connections.append([(channel, 1), (feedline_out, 0)])
                if channel_number == output_channel_number:
                    connections.append([(channel, 2), (port_out, 0)])

            # If channel in the middle of the filter-bank:
            else:
                connections.append(
                    [(interconnection_array[index - 1], 1), (channel, 0)]
                )
                connections.append([(channel, 1), (interconnection_array[index], 0)])
                if channel_number == output_channel_number:
                    connections.append([(channel, 2), (port_out, 0)])

        # If all channels have matched loads add port to end of feedline, else terminate feedline in match.
        if output_channel_number == 0:
            connections.append([(feedline_out, 1), (port_out, 0)])
        else:
            connections.append([(feedline_out, 1), (feedline_match, 0)])

    # Create Circuit instance using the full list of connections.
    circuit = rf.Circuit(connections)

    if plot_circuit_map:
        # Plot circuit map:
        circuit.plot_graph(
            network_labels=True,
            network_fontsize=15,
            port_labels=True,
            port_fontsize=15,
            edge_labels=True,
            edge_fontsize=10,
        )

    # Create network object from circuit:
    if output_channel_number == 0:
        return circuit.network.s[:, 0, 0], circuit.network.s[:, 0, 1]
    else:
        return circuit.network.s[:, 0, 1]


def main():
    # Define readout frequency band:
    frequency_band = rf.Frequency(start=110, stop=190, unit="GHz", npoints=1001)
    number_of_channels = 7
    position_array = np.linspace(0, 100, number_of_channels)
    dielectric_thickness_array = np.full(position_array.shape, 500e-9)
    etch_error_array = np.zeros(position_array.shape)
    target_f0_array = np.array([180.0, 170.0, 160.0, 150.0, 140.0, 130.0, 120.0])

    s_params = []
    for channel_number in range(number_of_channels + 1):

        if channel_number == 0:
            s11, s21 = create_filter_bank(
                frequency_band=frequency_band,
                target_f0_array=target_f0_array,
                etch_error_array=etch_error_array,
                dielectric_thickness_array=dielectric_thickness_array,
                loss_tangent=0.0,
                output_channel_number=channel_number,
                plot_circuit_map=False,
            )
            s_params.append(s11)
            s_params.append(s21)
        else:
            s_params.append(
                create_filter_bank(
                    frequency_band=frequency_band,
                    target_f0_array=target_f0_array,
                    etch_error_array=etch_error_array,
                    dielectric_thickness_array=dielectric_thickness_array,
                    loss_tangent=0.0,
                    output_channel_number=channel_number,
                    plot_circuit_map=False,
                )
            )
    s_params = np.array(s_params)
    plt.figure(figsize=(8, 6))
    for index, s_param in enumerate(s_params):
        plt.plot(
            frequency_band.f * 1e-9,
            np.abs(s_param) ** 2,
            label="S" + str(index + 1) + "1",
            linestyle="-",
        )
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()
