import scikit_filter_bank_simulation.util.filter_analysis_tools
from scikit_filter_bank_simulation.util.filter_bank_model_tools import (
    FilterBankModelTools,
)
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate

data_file = np.load("r200_coupling_capacitor_data_arrays.npz")
variable_cap_width_array = data_file["variableCapWidthArray"]
dielectric_thickness_array = data_file["dielectricThicknessArray"]
capacitance_array = data_file["capacitanceArray"]
parasitic_inductance_array = data_file["parasiticIndutanceArray"]

# # Interpolate data arrays for given variable_cap_width and dielectricThickness:
# capacitance = interpolate.griddata(
#     (variable_cap_width_array, dielectric_thickness_array),
#     capacitance_array,
#     (variable_cap_width, dielectric_thickness),
# )
# parasitic_l = interpolate.griddata(
#     (variable_cap_width_array, dielectric_thickness_array),
#     parasitic_inductance_array,
#     (variable_cap_width, dielectric_thickness),
# )

x = dielectric_thickness_array
y = variable_cap_width_array
z = capacitance_array

fig = plt.figure()

grid_x, grid_y = np.meshgrid(x, y)

ax = plt.axes(projection='3d')
ax.plot_trisurf(x, y, z,
                cmap='viridis', edgecolor='none');
ax.set_title('surface');
ax.set_xlabel("dielectric thickness (um)")
ax.set_ylabel("Capacitor Width (um)")
ax.set_zlabel("Capacitance (F)")
plt.show()

x = dielectric_thickness_array
y = variable_cap_width_array
z = parasitic_inductance_array

fig = plt.figure()

grid_x, grid_y = np.meshgrid(x, y)

ax = plt.axes(projection='3d')
ax.plot_trisurf(x, y, z,
                cmap='viridis', edgecolor='none');
ax.set_title('surface');
ax.set_xlabel("dielectric thickness (um)")
ax.set_ylabel("Capacitor Width (um)")
ax.set_zlabel("Parasitic Inductance (H)")
plt.show()

capacitance = interpolate.griddata(
    (variable_cap_width_array, dielectric_thickness_array),
    capacitance_array,
    (10, 0.3),
)
print(capacitance)
