import numpy as np
import skrf as rf
import matplotlib.pyplot as plt
from scikit_filter_bank_simulation.util.filter_bank_model_tools import (
    FilterBankModelTools,
)


def main():
    # Define readout frequency band:
    frequency_band = rf.Frequency(start=120, stop=180, unit="GHz", npoints=101)
    loss_tangents = [5e-4, 1.0e-3, 1.5e-3, 1e-3, 3e-3, 5e-3, 1e-2]
    length = 22000 * 1e-6

    plt.figure(figsize=(8, 6))

    for loss_tangent in loss_tangents:
        feedline_media = FilterBankModelTools.create_media(
            frequency_band=frequency_band,
            microstrip_width=2.5e-6,
            loss_tangent=loss_tangent,
            dielectric_thickness=300e-9,
        )

        input_line = feedline_media.line(d=length, unit="m", name="feedline line")

        plt.plot(
            frequency_band.f * 1e-9,
            100 * np.abs(input_line.s[:, 0, 1]) ** 2,
            label=f"{loss_tangent:.2e}",
            linestyle="-",
        )

    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 Throughput Power (%)")
    plt.legend(title="Loss Tangents")
    plt.show()


if __name__ == "__main__":
    main()
