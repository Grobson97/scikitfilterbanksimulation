"""
Script to simulate a filter bank of a given oversampling, the output graph shows
the total filter channel power transmission of the filter bank for the case with
no loss tangent, no etch error, and a constant dielectric thickness of 0.5um and
the filter bank with the specified loss tangent, random etch error per channel
and a varying dielectric thickness.
"""
import click

from complete_tolerancing.util.complete_tolerancing_tools import (
    CompleteTolerancingTools,
)


@click.command()
@click.option(
    "--save-figure",
    default=False,
    help="Should the output graph be saved to a file?",
)
@click.option(
    "--resolution",
    type=click.FLOAT,
    required=True,
    help="Filter-bank resolution (300 or 200)",
)
@click.option(
    "--oversampling",
    type=click.FLOAT,
    required=True,
    help="Channel oversampling amount",
)
@click.option(
    "--loss-tangent",
    type=click.FLOAT,
    required=True,
    help="Dielectric loss tangent",
)
@click.option(
    "--filter-spacing",
    type=click.FLOAT,
    required=True,
    help="Filter spacing relative to wavelength",
)
@click.option(
    "--frequency-min",
    type=click.INT,
    required=True,
    help="The frequency band minimum",
)
@click.option(
    "--frequency-max",
    type=click.INT,
    required=True,
    help="The frequency band maximum",
)
@click.option(
    "--start",
    type=click.INT,
    required=True,
    help="The start",
)
@click.option(
    "--stop",
    type=click.INT,
    required=True,
    help="The stop",
)
@click.option(
    "--number-of-points",
    type=click.INT,
    required=True,
    help="The number of points",
)
def complete_tolerancing(
    save_figure: bool,
    resolution: int,
    oversampling: float,
    loss_tangent: float,
    filter_spacing: float,
    frequency_min: int,
    frequency_max: int,
    start: int,
    stop: int,
    number_of_points: int,
):
    filter_bank_data = CompleteTolerancingTools.get_filter_bank_data(
        oversampling=oversampling,
        resolution=resolution,
        loss_tangent=loss_tangent,
        filter_spacing=filter_spacing,
        frequency_min=frequency_min,
        frequency_max=frequency_max,
        start=start,
        stop=stop,
        number_of_points=number_of_points,
    )

    CompleteTolerancingTools.plot_filter_bank_data(
        frequency_array=filter_bank_data.frequency_array,
        realistic_filter_bank_s_params=filter_bank_data.realistic_filter_bank_s_params,
        ideal_filter_bank_s_params=filter_bank_data.ideal_filter_bank_s_params,
        frequency_min=frequency_min,
        frequency_max=frequency_max,
        save_figure=save_figure,
    )


if __name__ == "__main__":
    complete_tolerancing()
