import time

import click
import numpy as np
import skrf as rf
from scipy import interpolate

from scikit_filter_bank_simulation.model.channel_components import ChannelComponents


class FilterBankModelTools:
    """Class of methods used to simulate a filterbank sprectrometer"""

    @staticmethod
    def create_target_f0_array(
        f_min: float, f_max: float, number_of_channels: int
    ) -> np.ndarray:
        """Returns a numpy array of log spaced target f0 values between the bounds of the frequency band.

        :param f_min: minimum frequency of the band
        :param f_max: maximum frequency of the band
        :param number_of_channels: number of channels in the filter, should be determined from resolving power and
        oversampling factor.
        :returns: A numpy array of log spaced target f0 values between the bounds of the frequency band.

        """
        target_f0_array = []
        fr = f_max
        while len(target_f0_array) < number_of_channels:
            target_f0_array.append(fr)
            fr = fr * np.exp(
                -(np.log(f_max) - np.log(f_min)) / (number_of_channels - 1)
            )

        return np.array(target_f0_array)

    @staticmethod
    def get_media_properties(microstrip_width: float, dielectric_thickness: float):
        """
        Loads the microstripDataArrays.npz file and extract the data arrays to then 2d interpolate the
        characteristic impedance, z0 and effective permittivity for a given microstrip_width and dielectricThickness.

        :param microstrip_width: Width of microstrip, use metres
        :param dielectric_thickness: Thickness of dielectric, use metres

        """

        # Remove comments if using r=300 simulation
        # Load the microstrip data file and extract arrays:
        # data_file = np.load("./r300MicrostripDataArrays.npz")
        # microstrip_width_array = data_file["microstripWidthArray"]
        # dielectric_thickness_array = data_file["dielectricThicknessArray"]
        # mean_port_z0_array = data_file["meanPortZ0Array"]
        # mean_port_permittivity_array = data_file["meanPortEeffArray"]

        # Block for R=200 simulation:
        data_file = np.load("r200_microstrip_data_arrays.npz")
        microstrip_width_array = data_file["microstrip_width_array"]
        dielectric_thickness_array = data_file["dielectric_thickness_array"]
        mean_port_z0_array = data_file["mean_port_z0_array"]
        mean_port_permittivity_array = data_file["mean_port_permittivity_array"]

        # Interpolate data for given width and thickness:
        z0_value = interpolate.griddata(
            (microstrip_width_array, dielectric_thickness_array),
            mean_port_z0_array,
            (microstrip_width, dielectric_thickness),
        )

        effective_permittivity = interpolate.griddata(
            (microstrip_width_array, dielectric_thickness_array),
            mean_port_permittivity_array,
            (microstrip_width, dielectric_thickness),
        )

        return z0_value, effective_permittivity

    @staticmethod
    def calculate_gamma(
        frequency_band: rf.Frequency, effective_permittivity: float, loss_tangent: float
    ) -> np.ndarray:
        """Calculates the array of propagation constants and gamma, of the microstrip transmission line
        at each frequency in the frequency_band.f.

        :param frequency_band: Defines the frequency band
        :param effective_permittivity: Effective permittivity for current microstrip parameters
        :param loss_tangent: Loss tangent of the media dielectric
        :returns: Array of propagation constants and gamma, of the microstrip transmission line at each frequency in the
            frequency_band.f.
        """
        v = 2.99e8 / np.sqrt(effective_permittivity)
        alpha = (np.pi * frequency_band.f / v) * loss_tangent
        beta = 2 * np.pi * frequency_band.f / v

        gamma = alpha + 1j * beta

        return gamma

    @staticmethod
    def create_media(
        frequency_band: rf.Frequency,
        microstrip_width: float,
        loss_tangent: float,
        dielectric_thickness: float,
    ) -> rf.Media:
        """Creates and returns an instance of the rf.Media.DefinedGammaZ0 class for the defined frequency_band to
        represent
        a microstrip media of the given width and dielectric loss tangent.
        The media Z0 and gamma arrays are obtained by interpolating the port Z0 and effective_permittivity from sonnet
        simulations output files of inverted microstrip transmission line for different width microstrips and
        thicknesses of SiN.

        :param frequency_band: Start and stop must be within the frequency bounds of the sonnet simulation data
        :param microstrip_width: Width of the microstrip transmission line. Must be in units of metres
        :param loss_tangent: Loss tangent of dielectric
        :param dielectric_thickness: Thickness of dielectric layer

        """

        z0, effective_permittivity = FilterBankModelTools.get_media_properties(
            microstrip_width, dielectric_thickness
        )

        gamma_array = FilterBankModelTools.calculate_gamma(
            frequency_band=frequency_band,
            effective_permittivity=effective_permittivity,
            loss_tangent=loss_tangent,
        )

        microstrip_media = rf.media.DefinedGammaZ0(
            frequency=frequency_band, z0=z0, gamma=gamma_array
        )

        return microstrip_media

    @staticmethod
    def create_coupling_capacitor(
        microstrip_media: rf.Media,
        variable_cap_width: float,
        dielectric_thickness: float,
        filter_resolution: float,
    ) -> rf.network:
        """Returns the two port rf.Network object of a coupling capacitor modelled as a series combination of an
        inductor,
        capacitor and inductor.
        Capacitance and inductance values are obtained from a 2d interpolation of the data arrays in
        couplingCapacitorDataArrays.npz.
        These arrays were obtained from fits to sonnet simulation data for the coupling capacitor.
        NB: Use unscaled units, e.g use metres, not micrometers.

        :param microstrip_media: Microstrip media for the capacitors
        :param variable_cap_width: Width of the variable width capacitor
        :param dielectric_thickness: thickness of dielectric material. Must be 550nm, 500nm, 450nm. Uses SI units
        :param filter_resolution: Resolution of the filter.
        """
        # Load the microstrip data file and extract arrays:

        data_file = np.load("./r300_coupling_capacitor_data_arrays.npz")
        if filter_resolution == 200:
            data_file = np.load("./r200_coupling_capacitor_data_arrays.npz")
            variable_cap_width = variable_cap_width * 1e6
            dielectric_thickness = dielectric_thickness * 1e6
        variable_cap_width_array = data_file["variableCapWidthArray"]
        dielectric_thickness_array = data_file["dielectricThicknessArray"]
        capacitance_array = data_file["capacitanceArray"]
        parasitic_inductance_array = data_file["parasiticIndutanceArray"]

        # Interpolate data arrays for given variable_cap_width and dielectricThickness:
        capacitance = interpolate.griddata(
            (variable_cap_width_array, dielectric_thickness_array),
            capacitance_array,
            (variable_cap_width, dielectric_thickness),
        )
        parasitic_l = interpolate.griddata(
            (variable_cap_width_array, dielectric_thickness_array),
            parasitic_inductance_array,
            (variable_cap_width, dielectric_thickness),
        )

        capacitor = microstrip_media.capacitor(C=capacitance)
        inductor = microstrip_media.inductor(L=parasitic_l)

        return inductor**capacitor**inductor

    @staticmethod
    def create_filter_channel(
        channel_number: int,
        input_microstrip_media: rf.Media,
        output_microstrip_media: rf.Media,
        cap_in: float,
        cap_out: float,
        dielectric_thickness: float,
        res_length: float,
        resolution: float,
        input_length=8.25e-6,
        output_length=19.25e-6,
    ) -> rf.Network:
        """Creates a two port network object which represents a filter channel.
        NB: All dimensions and variables should be in unscaled units. e.g. Use metres not
        micrometers.

        :param channel_number: Identifies the channel, network object will have the name: 'CH' + str(channel_number)
        :param input_microstrip_media: Microstrip media to use for input line and resonator
        :param output_microstrip_media: Microstrip media to use for output line
        :param cap_in: Width of input capacitor
        :param cap_out: Width of output capacitor
        :param dielectric_thickness: Thickness of dielectric layer
        :param res_length: Length of resonator in metres
        :param resolution: Resolution of the filter.
        :param input_length: Length of line from feedline to first capacitor (Default value = 8.25e-6)
        :param output_length: Length of line from output capacitor to channel port (Default value = 19.25e-6)
        :returns: A two port network object which represents a filter channel

        """

        input_t_line_ntwk = input_microstrip_media.line(
            d=input_length, unit="m", name="TL-In" + str(channel_number)
        )

        coupling_cap_in_ntwk = FilterBankModelTools.create_coupling_capacitor(
            microstrip_media=input_microstrip_media,
            variable_cap_width=cap_in,
            dielectric_thickness=dielectric_thickness,
            filter_resolution=resolution
        )

        resonator_ntwk = input_microstrip_media.line(
            d=res_length, unit="m", name="Res" + str(channel_number)
        )

        coupling_cap_out_ntwk = FilterBankModelTools.create_coupling_capacitor(
            microstrip_media=input_microstrip_media,
            variable_cap_width=cap_out,
            dielectric_thickness=dielectric_thickness,
            filter_resolution=resolution,
        )

        output_t_line_ntwk = output_microstrip_media.line(
            d=output_length, unit="m", name="TL-Out" + str(channel_number)
        )

        channel_ntwk = (
            input_t_line_ntwk
            ** coupling_cap_in_ntwk
            ** resonator_ntwk
            ** coupling_cap_out_ntwk
            ** output_t_line_ntwk
        )

        channel_ntwk.name = "CH" + str(channel_number)

        return channel_ntwk

    @staticmethod
    def calculate_res_len_correction(target_f0: float) -> float:
        """Calculates the correction required for the resonator length at the
        given targetF0 value. This is done via interpolating values that were
        determined from fitting to sonnet data.
        NB: The corrections were fit for filters with the designed variables:
        SiN thickness = 500nm, media linewidths = 2um, 2.5um 4.5um. Hence
        simulations likely to be increasingly less accurate the further from these
        values.

        :param target_f0: Target resonant frequency of the filter being built
        :returns: The correction required for the resonator length at the given targetF0 value

        """

        correction_array = np.array(
            [
                -4.072522538978163e-06,
                -4.42475698192446e-06,
                -3.6848867170924393e-06,
                -4.1346215224891125e-06,
                -3.1045675534802086e-06,
                -3.590536386877119e-06,
                -3.0398852972624013e-06,
                -2.5257621389940255e-06,
                -2.24554070160643e-06,
                -2.493695125060924e-06,
                -2.03789248964803e-06,
                -1.61115250718602e-06,
                -1.1143009110980984e-06,
                -8.763405145995051e-07,
                -5.01691903354029e-07,
                -5.250230779019205e-07,
                -4.6330992331533594e-07,
                -6.301521133789123e-07,
                1.0000000000715377e-09,
                3.423138996891202e-08,
                2.8822544958818104e-07,
                8.097162806559158e-07,
                1.272789448493836e-06,
            ]
        )

        target_f0_array = np.array(
            [
                120.0,
                122.0,
                124.0,
                126.0,
                132.0,
                134.0,
                136.0,
                138.0,
                142.0,
                144.0,
                145.0,
                146.0,
                150.0,
                152.0,
                154.0,
                155.0,
                156.0,
                158.0,
                164.0,
                166.0,
                168.0,
                178.0,
                180.0,
            ]
        )

        correction = np.interp(target_f0, target_f0_array, correction_array)
        return correction

    @staticmethod
    def calculate_cap_in_correction(target_f0: float) -> float:
        """Returns the correction required for capIn at the given targetF0 value. This is done via interpolating values
        that were determined from fitting to sonnet data.
        NB: The corrections were fit for filters with the designed variables:
        SiN thickness = 500nm, media linewidths = 2um, 2.5um 4.5um.
        Hence simulations likely to be increasingly less accurate the further from these values.

        :param target_f0: Target resonant frequency of the filter being built
        """
        cap_in_correction_array = np.array(
            [
                -7.100610541502722e-06,
                -6.898422678988768e-06,
                -6.70814952572922e-06,
                -6.290267852986109e-06,
                -5.894920721789237e-06,
                -5.780098730556124e-06,
                -5.40972638008088e-06,
                -5.3418164077888165e-06,
                -4.9470636243333174e-06,
                -4.941328105953877e-06,
                -4.780928678122513e-06,
                -4.914112953663105e-06,
                -4.572894210792917e-06,
                -4.595426865486842e-06,
                -4.312646825312908e-06,
                -4.156391416459759e-06,
                -4.317162241054553e-06,
                -4.044135808926437e-06,
                -3.877582727426629e-06,
                -3.6180216438263134e-06,
                -3.6819578414392987e-06,
                -3.1722500704327504e-06,
                -3.291949983119448e-06,
            ]
        )

        target_f0_array = np.array(
            [
                120.0,
                122.0,
                124.0,
                126.0,
                132.0,
                134.0,
                136.0,
                138.0,
                142.0,
                144.0,
                145.0,
                146.0,
                150.0,
                152.0,
                154.0,
                155.0,
                156.0,
                158.0,
                164.0,
                166.0,
                168.0,
                178.0,
                180.0,
            ]
        )

        correction = np.interp(target_f0, target_f0_array, cap_in_correction_array)
        return correction

    @staticmethod
    def calculate_cap_out_correction(target_f0: float) -> float:
        """Returns the correction required for the capOut at the given targetF0 value. This is done via interpolating
        values that were determined from fitting to sonnet data.
        NB: The corrections were fit for filters with the designed variables:
        SiN thickness = 500nm, media linewidths = 2um, 2.5um 4.5um. Hence simulations likely to be increasingly less
        accurate the further from these values.

        :param target_f0: Target resonant frequency of the filter being built
        """

        cap_out_correction_array = np.array(
            [
                -4.597621663231297e-06,
                -4.465388674454441e-06,
                -4.356343212706362e-06,
                -4.273625860095653e-06,
                -3.8506171617003945e-06,
                -3.5319268444043214e-06,
                -3.5106739265305964e-06,
                -3.4349007134971976e-06,
                -3.1269245766490194e-06,
                -2.869326102474588e-06,
                -3.023855111336641e-06,
                -2.8863158398161596e-06,
                -2.6352471734081315e-06,
                -2.7051732241597117e-06,
                -2.4444064474319386e-06,
                -2.349669671457104e-06,
                -2.5439940470829497e-06,
                -2.297881190286631e-06,
                -1.9455222557455544e-06,
                -1.9936179249298144e-06,
                -1.796767334202283e-06,
                -1.394070626285909e-06,
                -1.4858023873456027e-06,
            ]
        )

        target_f0_array = np.array(
            [
                120.0,
                122.0,
                124.0,
                126.0,
                132.0,
                134.0,
                136.0,
                138.0,
                142.0,
                144.0,
                145.0,
                146.0,
                150.0,
                152.0,
                154.0,
                155.0,
                156.0,
                158.0,
                164.0,
                166.0,
                168.0,
                178.0,
                180.0,
            ]
        )

        correction = np.interp(target_f0, target_f0_array, cap_out_correction_array)
        return correction

    @staticmethod
    def create_filter_bank_circuit(
        frequency_band: rf.Frequency,
        target_f0_array: np.ndarray,
        resolution: float,
        etch_error_array: np.ndarray,
        dielectric_thickness_array: np.ndarray,
        loss_tangent: float,
        output_channel_number: int,
        filter_spacing=0.25,
        plot_circuit_map=False,
    ) -> rf.Circuit:
        """Creates a two port rf.Network object representing a filter bank. The output port is placed on the desired
        output channel. All other channels are terminated in a matched load.

        :param frequency_band: instance of the frequency band defining each frequency point that will be evaluated
        :param target_f0_array: array of target F0 values used to define the geometry of each filter.
        :param resolution: Resolution of the filterbank. Can either be 200 or 300.
        :param etch_error_array: Array of values that can be added to geometry dimensions due to over or under etching,
        e.g increase line widths. Must be same length as target_f0 array. Each value is the etch error that will be used
        for that corresponding channel
        :param dielectric_thickness_array: array of values for each channels dielectric thickness. Must be same length
        as target_f0 array
        :param loss_tangent: Dielectric loss tangent
        :param output_channel_number: The port number to be used as the output. All other channels will be terminated in a
        matched load. If 0, all channels are terminated in a matched load and output port is at the other end of the
        feedline.
        :param filter_spacing: The physical distance between neighbouring filters ("interconnection length") relative to
        the wavelength. e.g. 0.25 is quarter wave spaced. 2.0 would be double wavelength spaced, etc.
        :param plot_circuit_map: Option to plot the map of the filter-bank circuit using the rf.Circuit.plot_graph()
        method (Default value = False)

        """

        dielectric_thickness = 500e-9
        if resolution == 200:
            dielectric_thickness = 300e-9

        feedline_media = FilterBankModelTools.create_media(
            frequency_band=frequency_band,
            microstrip_width=2.5e-6,
            loss_tangent=loss_tangent,
            dielectric_thickness=dielectric_thickness,
        )

        # Create parasitic capacitance to ground network object at first channel connection
        parasitic_cap = feedline_media.capacitor(C=-7.951817997309143e-15)
        # Add a short after parasitic capacitor
        parasitic_cap_to_gnd = parasitic_cap ** feedline_media.short()
        parasitic_cap_to_gnd = feedline_media.shunt(
            parasitic_cap_to_gnd, name="Parasitic C1"
        )

        feedline_in = feedline_media.line(110e-6, "m", name="FeedIn")
        feedline_in = feedline_in**parasitic_cap_to_gnd
        feedline_out = feedline_media.line(110e-6, "m", name="FeedOut")

        filter_bank_ntwk_1 = feedline_in

        for index, target_f0 in enumerate(target_f0_array):
            channel_number = index + 1
            # select etch Error and dielectric thickness for the new channel.
            etch_error = etch_error_array[index]
            dielectric_thickness = dielectric_thickness_array[index]

            channel_components = ChannelComponents(
                etch_error=etch_error,
                dielectric_thickness=dielectric_thickness,
                frequency_band=frequency_band,
                loss_tangent=loss_tangent,
                target_f0=target_f0,
                resolution=resolution,
            )

            # Create 2-port network object for the channel:
            input_length = 8.25e-6
            output_length = 19.25e-6
            if resolution == 200:
                input_length = 8.3e-6
                output_length = 17.1e-6
            channel_ntwk = FilterBankModelTools.create_filter_channel(
                channel_number=index + 1,
                input_microstrip_media=channel_components.resonator_media,
                output_microstrip_media=channel_components.channel_out_media,
                cap_in=channel_components.cap_in,
                cap_out=channel_components.cap_out,
                dielectric_thickness=dielectric_thickness,
                res_length=channel_components.res_length,
                resolution=resolution,
                input_length=input_length,
                output_length=output_length,
            )

            # If not the desired output channel, terminate channel in matched load. (Creates 1 port network)
            if channel_number != output_channel_number:
                channel_ntwk = (
                    channel_ntwk ** channel_components.channel_out_media.match()
                )

            # Convert channel network into a shunted network. increases number of ports by 1:
            shunt_channel_ntwk = feedline_media.shunt(
                channel_ntwk, name="CH" + str(index + 1)
            )

            # renumber ports for the 3 port channel network has the following indices:
            # 0: feedline input
            # 1: feedline output
            # 2: channel output
            if shunt_channel_ntwk.nports == 3:
                shunt_channel_ntwk.renumber([0, 1, 2], [2, 0, 1])

            # Create parasitic capacitance to ground network object at each filter.
            parasitic_cap = feedline_media.capacitor(C=-7.951817997309143e-15)

            # Add a short after parasitic capacitor
            parasitic_cap_to_gnd = parasitic_cap ** feedline_media.short()
            parasitic_cap_to_gnd = feedline_media.shunt(
                parasitic_cap_to_gnd, name="Parasitic C-" + str(index + 2)
            )

            # Create microstrip network object to place between filters:
            interconnection_ntwk = feedline_media.line(
                d=2
                * filter_spacing
                * channel_components.res_length,  # times 2 since res_length is half a wavelength
                unit="m",
                name="Interconnect-" + str(index + 1),
            )
            interconnection_ntwk = interconnection_ntwk**parasitic_cap_to_gnd

            if output_channel_number != 0:
                if channel_number < output_channel_number:
                    filter_bank_ntwk_1 = (
                        filter_bank_ntwk_1**shunt_channel_ntwk**interconnection_ntwk
                    )
                if channel_number == output_channel_number:
                    output_channel_ntwk = shunt_channel_ntwk
                    filter_bank_ntwk_2 = interconnection_ntwk
                if channel_number > output_channel_number:
                    filter_bank_ntwk_2 = (
                        filter_bank_ntwk_2**shunt_channel_ntwk**interconnection_ntwk
                    )

            if output_channel_number == 0:
                filter_bank_ntwk_1 = (
                    filter_bank_ntwk_1**shunt_channel_ntwk**interconnection_ntwk
                )

        if output_channel_number != 0:
            filter_bank_ntwk = rf.connect(filter_bank_ntwk_1, 1, output_channel_ntwk, 0)
            filter_bank_ntwk = rf.connect(
                filter_bank_ntwk,
                1,
                (filter_bank_ntwk_2**feedline_out ** feedline_media.match()),
                0,
            )
            return filter_bank_ntwk

        if output_channel_number == 0:
            return filter_bank_ntwk_1**feedline_out

    @staticmethod
    def get_filter_bank_s_params(
        frequency_band: rf.Frequency,
        target_f0_array: np.ndarray,
        resolution: float,
        etch_error_array: np.ndarray,
        dielectric_thickness_array: np.ndarray,
        loss_tangent: float,
        filter_spacing=0.25,
        plot_circuit_map=False,
    ) -> np.ndarray:
        """Creates a numpy array of all diagonal components of a filter-bank S parameter matrix. e.g. S11, S21....SN1.
        The indices of the s_params matrix represent the following:
        0: S11
        1: S21
        N: SN1, where N-2 is the filter channel number. e.g. index N=3 gives the throughput from port one to filter
        channel 1.
        Method uses a loop to create a new filter-bank each time with a different output port and extracts the
        throughput transmission S matrix, SN1.

        :param frequency_band: instance of the frequency band defining each frequency point that will be evaluated
        :param target_f0_array: array of target F0 values used to define the geometry of each filter.
        :param resolution: Resolution of the filterbank. Can either be 200 or 300.
        :param etch_error_array: Array of values that can be added to geometry dimensions due to over or under etching,
        e.g increase line widths. Must be same length as target_f0 array. Each value is the etch error that will be used
        for that corresponding channel
        :param dielectric_thickness_array: array of values for each channels dielectric thickness. Must be same length
        as target_f0 array
        :param loss_tangent: Dielectric loss tangent
        :param filter_spacing: The physical distance between neighbouring filters ("interconnection length") relative to
        the wavelength. e.g. 0.25 is quarter wave spaced. 2.0 would be double wavelength spaced, etc.
        :param plot_circuit_map: Option to plot the map of the filter-bank circuit using the rf.Circuit.plot_graph()
        method (Default value = False)

        """
        click.echo("Getting new filter bank S parameters...")

        s_params = []
        total = target_f0_array.size + 1
        for channel_number in range(total):
            time_channel = time.time()
            filter_bank = FilterBankModelTools.create_filter_bank_circuit(
                frequency_band=frequency_band,
                target_f0_array=target_f0_array,
                resolution=resolution,
                etch_error_array=etch_error_array,
                dielectric_thickness_array=dielectric_thickness_array,
                loss_tangent=loss_tangent,
                output_channel_number=channel_number,
                filter_spacing=filter_spacing,
                plot_circuit_map=plot_circuit_map,
            )
            s_array = filter_bank.s
            if channel_number == 0:
                s_params.append(s_array[:, 0, 0])  # S11 of full filter-bank.
            s_params.append(s_array[:, 0, 1])  # SN1 of full filter-bank.

            click.echo(
                f"Got S params for channel {channel_number} of {total-1} ({(time.time() - time_channel):.2f}s)"
            )

        return np.array(s_params)

    @staticmethod
    def get_filter_bank_s21(filter_bank: rf.Network) -> np.ndarray:
        """Returns the S21 of a filter bank network object created using the createFilterBank method.
        This is the throughput S parameter array from portIn to portOut, i.e. from antenna to the end of the feedline.

        :param filter_bank: Network instance representing the filterBank. Must be created using the createFilterBank
        method

        """

        return filter_bank.s[:, 0, -1]

    @staticmethod
    def get_channel_transmission(
        filter_bank: rf.Network, channel_number: int
    ) -> np.ndarray:
        """Returns the S parameter array of the given filter channel in the filterBank network object which was created
        using the createFilterBank method. This is the throughput S parameter array from portIn to port at the end of
        the channel, i.e. from antenna to the channel output.

        :param filter_bank: Network instance representing the filterBank. Must be created using the createFilterBank
        method
        :param channel_number: Filter channel number where the channel closest to the antenna is channel 1 and the next
        would be channel 2, etc.
        """
        return filter_bank.s[:, 0, channel_number]
