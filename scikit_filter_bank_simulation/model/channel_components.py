import scikit_filter_bank_simulation.util.filter_bank_model_tools


class ChannelComponents:
    def __init__(
        self,
        etch_error,
        dielectric_thickness,
        frequency_band,
        loss_tangent,
        target_f0,
        resolution,
    ):
        # update microstrip media for new channel:
        self.feedline_media = scikit_filter_bank_simulation.util.filter_bank_model_tools.FilterBankModelTools.create_media(
            frequency_band=frequency_band,
            microstrip_width=2.5e-6 + etch_error,
            loss_tangent=loss_tangent,
            dielectric_thickness=dielectric_thickness,
        )

        self.resonator_media = scikit_filter_bank_simulation.util.filter_bank_model_tools.FilterBankModelTools.create_media(
            frequency_band=frequency_band,
            microstrip_width=2e-6 + etch_error,
            loss_tangent=loss_tangent,
            dielectric_thickness=dielectric_thickness,
        )

        output_width = 4.5e-6
        if resolution == 200:
            output_width = 3.0e-6
        self.channel_out_media = scikit_filter_bank_simulation.util.filter_bank_model_tools.FilterBankModelTools.create_media(
            frequency_band=frequency_band,
            microstrip_width=output_width + etch_error,
            loss_tangent=loss_tangent,
            dielectric_thickness=dielectric_thickness,
        )

        # Define parameters for the SingleFilter instance:
        # target_f0 = correctTargetF0(target_f0)
        if resolution == 200:   # Account for offset in f0's between target and simulated.
            target_f0 = (target_f0 + 4.58982510796065) / 0.9222330586790085

        res_w = (
            -5.69e-5 * target_f0**3 + 0.033 * target_f0**2 - 7.13 * target_f0 + 669
        )
        self.res_length = (
            12 + 2 * res_w
        ) * 1e-6 + scikit_filter_bank_simulation.util.filter_bank_model_tools.FilterBankModelTools.calculate_res_len_correction(
            target_f0
        )
        self.cap_in = (
            (0.14 * res_w - 10.0) * 1e-6
            + scikit_filter_bank_simulation.util.filter_bank_model_tools.FilterBankModelTools.calculate_cap_in_correction(
                target_f0
            )
            + etch_error
        )
        self.cap_out = (
            (0.121 * res_w - 10.7) * 1e-6
            + scikit_filter_bank_simulation.util.filter_bank_model_tools.FilterBankModelTools.calculate_cap_out_correction(
                target_f0
            )
            + etch_error
        )

        if resolution == 200:
            res_w = (target_f0 * 1e9 / (10 ** 1.31e01)) ** (-1 / 8.76e-01)
            self.res_length = (34.4 + 2 * res_w) * 1e-6 + etch_error
            self.cap_in = (1.54e-01 * res_w - 10.1) * 1e-6 + etch_error
            self.cap_out = (1.01e-01 * res_w - 7.11) * 1e-6 + etch_error
