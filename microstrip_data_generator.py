import skrf as rf
import matplotlib.pyplot as plt
import scikit_filter_bank_simulation.util.touchstone_tools as touchstone_tools
import numpy as np
import os

directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\Feedline\Microstrip\width_and_dielectric_thickness_variation"

fit_dictionary = {
    "dielectric_thickness": [],
    "microstrip_width": [],
    "mean_port_z0": [],
    "mean_port_permittivity": [],
}

for count, file in enumerate(os.listdir(directory)):
    filename = os.fsdecode(file)
    file_path = os.path.join(directory, filename)

    if ".s2p" in filename:
        # Read current touchstone file
        touchstone_file = rf.Touchstone(file_path)
        # Get touchstone variables from comments
        variables = touchstone_file.get_comment_variables()

        # Make string ccontaining file parameters:
        variables_string = ""
        for variable in variables:
            if variable == list(variables.keys())[-1]:
                variables_string += variable + "=" + str(variables[variable][0])
                continue
            variables_string += variable + "=" + str(variables[variable][0]) + ", "

        # Extract variables from header
        microstrip_width = float(variables["center_width"][0])
        dielectric_thickness = float(variables["dielectric_thickness"][0])

        # Renormalise port impedances.
        network = rf.Network(file_path)
        frequency = network.f

        # Extract mean port impedance and E effective
        mean_port_z0_result = touchstone_tools.get_mean_port_impedance(file_path=file_path, port_number="1")
        mean_port_z0 = complex(mean_port_z0_result[0], mean_port_z0_result[1])
        mean_port_permittivity = touchstone_tools.get_mean_port_permittivity(file_path=file_path, port_number="1")

        fit_dictionary["dielectric_thickness"].append(dielectric_thickness * 1e-6)
        fit_dictionary["microstrip_width"].append(microstrip_width * 1e-6)
        fit_dictionary["mean_port_z0"].append(mean_port_z0)
        fit_dictionary["mean_port_permittivity"].append(mean_port_permittivity)

        print(f"file {count} complete")

for key in fit_dictionary:
    fit_dictionary[key] = np.array(fit_dictionary[key])

np.savez(
    "r200_microstrip_data_arrays",
    microstrip_width_array=fit_dictionary["microstrip_width"],
    dielectric_thickness_array=fit_dictionary["dielectric_thickness"],
    mean_port_z0_array=fit_dictionary["mean_port_z0"],
    mean_port_permittivity_array=fit_dictionary["mean_port_permittivity"],
)
