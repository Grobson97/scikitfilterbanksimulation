import numpy as np
import skrf as rf
import matplotlib.pyplot as plt
from scikit_filter_bank_simulation.util.filter_bank_model_tools import (
    FilterBankModelTools,
)
import scikit_filter_bank_simulation.util.filter_analysis_tools as filter_analysis_tools


def main():
    # Define readout frequency band:
    frequency_band = rf.Frequency(start=110, stop=190, unit="GHz", npoints=101)
    loss_tangent = 1e-3
    target_f0 = 160
    resolution = 300
    resonator_media = FilterBankModelTools.create_media(
        frequency_band=frequency_band,
        microstrip_width=2.0e-6,
        loss_tangent=loss_tangent,
        dielectric_thickness=500e-9,
    )
    feedline_media = FilterBankModelTools.create_media(
        frequency_band=frequency_band,
        microstrip_width=2.5e-6,
        loss_tangent=loss_tangent,
        dielectric_thickness=500e-9,
    )
    channel_out_media = FilterBankModelTools.create_media(
        frequency_band=frequency_band,
        microstrip_width=4.6e-6,
        loss_tangent=loss_tangent,
        dielectric_thickness=500e-9,
    )

    res_w = -5.69e-5 * target_f0**3 + 0.033 * target_f0**2 - 7.13 * target_f0 + 669
    res_length = (
        12 + 2 * res_w
    ) * 1e-6 + FilterBankModelTools.calculate_res_len_correction(target_f0)
    cap_in = (
        0.14 * res_w - 10.0
    ) * 1e-6 + FilterBankModelTools.calculate_cap_in_correction(target_f0)
    cap_out = (
        0.121 * res_w - 10.7
    ) * 1e-6 + FilterBankModelTools.calculate_cap_out_correction(target_f0)
    if resolution == 200:
        res_w = (target_f0 * 1e9 / (10 ** 1.31e01)) ** (-1 / 8.76e-01)
        res_length = (34.4 + 2 * res_w) * 1e-6
        cap_in = (1.54e-01 * res_w - 10.1) * 1e-6
        cap_out = (1.01e-01 * res_w - 7.11) * 1e-6

    input_line = feedline_media.line(d=20e-6, unit="m", name="input line")
    output_line = feedline_media.line(d=20e-6, unit="m", name="output line")

    port_in = rf.Circuit.Port(
        feedline_media.frequency, name="port in", z0=feedline_media.z0
    )
    # Create output port at end of filter-bank.
    port_out = rf.Circuit.Port(frequency_band, name="port out", z0=feedline_media.z0)

    # Create port to put at the end of channel:
    channel_port = rf.Circuit.Port(
        frequency_band, name="channel port", z0=channel_out_media.z0
    )

    # Create parasitic capacitance to ground network object at each filter.
    parasitic_cap = feedline_media.capacitor(
        C=-7.951817997309143e-15, name="Parasitic C"
    )

    parasitic_cap_to_gnd = parasitic_cap ** feedline_media.short()

    ################################################################################
    # Section to build a single filter channel the old way

    filter_channel = FilterBankModelTools.create_filter_channel(
        channel_number=1,
        input_microstrip_media=resonator_media,
        output_microstrip_media=channel_out_media,
        cap_in=cap_in,
        cap_out=cap_out,
        dielectric_thickness=500e-9,
        res_length=res_length,
        resolution=200
    )

    shunt_channel_ntwk = feedline_media.shunt(filter_channel, name="Channel")

    connections = [
        [(port_in, 0), (input_line, 0)],
        [(input_line, 1), (shunt_channel_ntwk, 1), (parasitic_cap_to_gnd, 0)],
        [(shunt_channel_ntwk, 0), (channel_port, 0)],
        [(shunt_channel_ntwk, 2), (output_line, 0)],
        [(output_line, 1), (port_out, 0)],
    ]

    circuit = rf.Circuit(connections=connections)
    final_network = circuit.network

    ################################################################################
    # Section to build a shingle filter channel the new way.
    desired_channel = True

    if desired_channel:
        filter_channel_matched = filter_channel
    else:
        filter_channel_matched = filter_channel ** channel_out_media.match()

    shunt_channel_ntwk_matched = feedline_media.shunt(
        filter_channel_matched, name="Matched Channel"
    )

    shunt_parasitic_cap_to_gnd = feedline_media.shunt(
        parasitic_cap_to_gnd, name="parasitic C"
    )

    if desired_channel:
        final_network_matched = rf.connect(input_line, 1, shunt_channel_ntwk_matched, 1)
        final_network_matched = final_network_matched
    else:
        final_network_matched = input_line**shunt_channel_ntwk_matched
        final_network_matched = (
            final_network_matched**shunt_parasitic_cap_to_gnd**output_line
        )

    plt.figure(figsize=(8, 6))
    plt.plot(
        frequency_band.f * 1e-9,
        np.abs(final_network.s[:, 0, 1]) ** 2,
        label="Filter Channel",
        linestyle="-",
    )
    plt.plot(
        frequency_band.f * 1e-9,
        np.abs(final_network_matched.s[:, 0, 1]) ** 2,
        label="Matched",
        linestyle="--",
    )

    plt.legend()
    plt.show()
    #
    filter_analysis_tools.fit_filter_s31_lorentzian(
        frequency_array=frequency_band.f,
        data_array=np.abs(final_network_matched.s[:, 0, 1]) ** 2,
        q_filter_guess=300,
        qi_guess=1000,
        f0_guess=frequency_band.f[np.argmax(np.abs(final_network_matched.s[:, 0, 1]) ** 2)],
        normalise=False,
        plot_graph=True,
        plot_db=False
    )


if __name__ == "__main__":
    main()
