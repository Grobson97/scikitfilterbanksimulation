import numpy as np
import skrf as rf
import matplotlib.pyplot as plt

from scikit_filter_bank_simulation.util.filter_bank_model_tools import (FilterBankModelTools)
import scikit_filter_bank_simulation.util.filter_analysis_tools as filter_analysis_tools

def get_simulated_f0(target_f0):
    # Define readout frequency band:
    frequency_band = rf.Frequency(start=80, stop=190, unit="GHz", npoints=1001)
    loss_tangent = 0.0
    target_f0 = target_f0
    resolution = 200
    dielectric_thickness = 300e-9
    to_port = 3  # For Channel output use 3, for feedline output use 2.

    resonator_media = FilterBankModelTools.create_media(
        frequency_band=frequency_band,
        microstrip_width=2.0e-6,
        loss_tangent=loss_tangent,
        dielectric_thickness=dielectric_thickness,
    )
    feedline_media = FilterBankModelTools.create_media(
        frequency_band=frequency_band,
        microstrip_width=2.5e-6,
        loss_tangent=loss_tangent,
        dielectric_thickness=dielectric_thickness,
    )
    channel_out_media = FilterBankModelTools.create_media(
        frequency_band=frequency_band,
        microstrip_width=3.0e-6,
        loss_tangent=loss_tangent,
        dielectric_thickness=dielectric_thickness,
    )

    res_w = -5.69e-5 * target_f0 ** 3 + 0.033 * target_f0 ** 2 - 7.13 * target_f0 + 669
    res_length = (
                         12 + 2 * res_w
                 ) * 1e-6 + FilterBankModelTools.calculate_res_len_correction(target_f0)
    cap_in = (
                     0.14 * res_w - 10.0
             ) * 1e-6 + FilterBankModelTools.calculate_cap_in_correction(target_f0)
    cap_out = (
                      0.121 * res_w - 10.7
              ) * 1e-6 + FilterBankModelTools.calculate_cap_out_correction(target_f0)
    if resolution == 200:
        res_w = (target_f0 * 1e9 / (10 ** 1.31e01)) ** (-1 / 8.76e-01)
        res_length = (34.4 + 2 * res_w) * 1e-6
        cap_in = (1.54e-01 * res_w - 10.1) * 1e-6
        cap_out = (1.01e-01 * res_w - 7.11) * 1e-6

    # Create parasitic capacitance to ground network object at each filter.
    parasitic_cap = feedline_media.capacitor(
        C=-7.951817997309143e-15, name="Parasitic C"
    )

    # Add a short after parasitic capacitor
    parasitic_cap_to_gnd = parasitic_cap ** feedline_media.short()
    parasitic_cap_to_gnd = feedline_media.shunt(
        parasitic_cap_to_gnd, name="Parasitic C1"
    )

    feedline_in = feedline_media.line(110e-6, "m", name="FeedIn")
    feedline_in = feedline_in ** parasitic_cap_to_gnd
    feedline_out = feedline_media.line(110e-6, "m", name="FeedOut")

    # Create 2-port network object for the channel:
    input_length = 8.25e-6
    output_length = 19.25e-6
    if resolution == 200:
        input_length = 8.3e-6
        output_length = 17.1e-6
    channel_ntwk = FilterBankModelTools.create_filter_channel(
        channel_number=0,
        input_microstrip_media=resonator_media,
        output_microstrip_media=channel_out_media,
        cap_in=cap_in,
        cap_out=cap_out,
        dielectric_thickness=dielectric_thickness,
        res_length=res_length,
        resolution=resolution,
        input_length=input_length,
        output_length=output_length,
    )

    # Convert channel network into a shunted network. increases number of ports by 1:
    shunt_channel_ntwk = feedline_media.shunt(channel_ntwk, name="CH0")

    # renumber ports for the 3 port channel network has the following indices:
    # 0: feedline input
    # 1: feedline output
    # 2: channel output

    shunt_channel_ntwk.renumber([0, 1, 2], [2, 0, 1])

    # connect input with 3 port shunt channel:
    filter_channel_network = rf.connect(feedline_in, 1, shunt_channel_ntwk, 0)

    if to_port == 2:
        filter_channel_network = rf.connect(filter_channel_network, 2, channel_out_media.match(), 0)
        filter_channel_network = filter_channel_network ** feedline_out

    if to_port == 3:
        feedline_out = feedline_out ** feedline_media.match()
        filter_channel_network = rf.connect(filter_channel_network, 1, feedline_out, 0)

    fit_results = filter_analysis_tools.fit_filter_s31_lorentzian(
        frequency_array=frequency_band.f,
        data_array=np.abs(filter_channel_network.s[:, 0, 1]) ** 2,
        q_filter_guess=300,
        qi_guess=10000,
        f0_guess=frequency_band.f[np.argmax(np.abs(filter_channel_network.s[:, 0, 1]) ** 2)],
        normalise=False,
        plot_graph=False,
        plot_db=False
    )

    return fit_results["f0"][0]

def main():

    target_f0_array = [120, 130, 140, 150, 160, 170, 180]
    simulated_f0_array = []

    for f0 in target_f0_array:
        simulated_f0 = get_simulated_f0(f0)
        simulated_f0_array.append(simulated_f0 * 1e-9)

    target_f0_array = np.array(target_f0_array)
    simulated_f0_array = np.array(simulated_f0_array)

    linear_fit_result = filter_analysis_tools.fit_linear_regression(
        target_f0_array,
        simulated_f0_array,
        plot_graph=True,
    )

    x = 150
    y = linear_fit_result.slope * x + linear_fit_result.intercept

    required_x = (y - linear_fit_result.intercept) / linear_fit_result.slope


    print("fhjkfs")

if __name__ == "__main__":
    main()
